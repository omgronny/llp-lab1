#pragma once
#include <coroutine>
#include <iostream>

class generator {
public:
    struct promise_type {

        using suspend_never = std::suspend_never;
        using suspend_always = std::suspend_always;
        using handle = std::coroutine_handle<promise_type>;
        std::string value;
        /* создание экземпляра класса generator
         * да, этим занимается promise!
         */
        auto get_return_object() noexcept {
            return generator{handle::from_promise(*this)};
        }
        /* suspend_never говорит C++ выполнить корутину
         * до первого вызова co_yield/co_return сразу в момент её создания
         */
        suspend_never initial_suspend() noexcept { return {}; }
        /* suspend_always указывает С++ придержать разрушение
         * корутины в момент её завершения. Это необходимо, чтобы не
         * потерять возможность обращаться к promise и handle
         * после её завершения. В противном случае вы даже не сможете
         * проверить done() см. ниже
         */
        suspend_always final_suspend() noexcept { return {}; }
        /* наши генераторы не будут ничего возвращать
         * через co_return, только через co_yield
         */
        void return_void() noexcept {}
        /* обработка `co_yield value` внутри генератора */
        suspend_always yield_value(std::string& v) noexcept {
            value = v;
            return {};
        }
        /* на первом этапе мы не обрабатываем исключения внутри генераторов*/
        void unhandled_exception() { std::terminate(); }
    };
    /* Поскольку finial_suspend придерживает уничтожение корутины
     * нам необходимо уничтожить её вручную
     */
    ~generator() noexcept { m_coro.destroy(); }

    /* iterator и методы begin(), end() необходимы для компиляции цикла
     * for (auto value: generator(0, 10)), описание логики работы range
     * base for выходит за рамки данной статьи
     */
    class iterator {
    public:
        bool operator != (iterator second) const {
            return m_self != second.m_self;
        }

        iterator & operator++() {
            /* воззобновить выполнение корутины - генератора */
            m_self->m_coro.resume();
            /* проверяем, завершилась ли корутина, если бы не final_suspend
             * возвращающий suspend_always - нас бы ждал облом
             */
            if (m_self->m_coro.done()) {
                m_self = nullptr;
            }
            return *this;
        }

        std::string operator*() {
            /* достаем значение напрямую из promise */
            return m_self->m_coro.promise().value;
        }

    private:
        iterator(generator *self): m_self{self} {}
        generator *m_self;
        friend class generator;
    };

    /* первое значение корутины уже вычитано благодаря
     * inital_suspend, возвращающим suspend_never
     */
    iterator begin() { return iterator{m_coro.done() ? nullptr : this}; }
    iterator end() { return iterator{nullptr}; }
private:
    promise_type::handle m_coro;
    /* конструктор, который будет вызван из get_return_object */
    explicit generator(promise_type::handle coro) noexcept: m_coro{coro} {}
};