//
// Created by omgronny on 15.09.2022.
//

#ifndef LAB1_CPP_COMMAND_EXECUTOR_H
#define LAB1_CPP_COMMAND_EXECUTOR_H

#include <string>
#include <utility>
#include <vector>
#include <optional>

#include "../utils.h"

// insert table1 (1, 228, hello, world)

// select from table2 where id=228
// select from table2 where weight>10

// update table3 where size<10

// delete from table4 where size>=3

// create table5 (id, f1, f2, f3)

/*
 * Это высокоуровневая абстракция над файлом
 * Тут скорее описываются логические действия, проверка корректности и т.д.
 * а вся грязная работа прячется в file_consistency_keeper
 */

#include "../file_workers/file_consistency_keeper.h"

class command_executor {

    // file ?
    file_consistency_keeper fck;
    uint64_t line_size_constraint;

    static std::string to_string(type);
    std::optional<std::pair<std::vector<std::string>, uint64_t>> type_check(const std::string& table_name,
                                                       const std::string& column,
                                                       type as_type,
                                                       compare_by cmp);

public:

    explicit command_executor(const char*, uint64_t);
    // command_executor(file);

    bool insert(const insert_command&);
    returned_data select(const select_command&);

    returned_data join_select(const join_select_command&);

    bool update(const update_command&);
    bool _delete(const delete_command&);

    bool create(const create_command&);
    bool drop(const drop_command&);

    ~command_executor() = default;

};


#endif //LAB1_CPP_COMMAND_EXECUTOR_H
