#include <iostream>
#include "commands/command_executor.h"
#include "test/test.hpp"

#include <time.h>

/*
 * test results:
 * 1) добавляем по 10000 в цикле 10 раз (инсерт + селект), файл 18мб
 * 496 433 434 411 410 433 432 441 415 607
 * 1399 1551 1542 1512 1563 1664 1793 1704 2086 2203
 *
 * 2) добавляем по 10000 в цикле 10 раз (инсерт + селект), файл 22мб, тест сразу после первого
 * 479 508 427 608 437 417 413 606 480 486
 * 2200 1997 2079 2234 2136 2056 2139 2716 2878 2322
 *
 * 3) селект в файле 27мб после оптимизаций
 * 2211 2206 2089 2099 2098 2088 2128 2115 2141 2064
 */

/*
 * delete
 *
 * logical number of elements: 20k and +10k after every run
 * file size: 572 953 1400 1700 2100 2500 2800 3200 3600 4000 4300
 * time: 80 179 195 248 245 296 320 313 372 353 380
 *
 * file size: 4300 4300 4300 4300 4300 4300 4300 4300 4300 4300
 * time: 100 175 199 228 242 263 292 311 330 353
 */

int main() {

    command_executor commandExecutor("../db.txt", 50);

    //create(commandExecutor);

    insert_simple(commandExecutor);

    //insert_big(commandExecutor);

    select(commandExecutor);

    //_delete(commandExecutor);

    //stress_select(commandExecutor);

    //stress_insert_and_select(commandExecutor);
    //stress_insert_and_update(commandExecutor);

    //stress_delete_and_file_size(commandExecutor);

    //test_float(commandExecutor);

    return 0;
}




